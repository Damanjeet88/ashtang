<?php

class About extends MY_Controller {

    public function index(){
        $title = "About Us";
        $this->load->view('includes/header', array('title' => $title));
        $this->load->view('pages/about');
        $this->load->view('includes/footer');
    }
}
