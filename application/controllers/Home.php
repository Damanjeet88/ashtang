<?php

class Home extends MY_Controller {

    public function index(){
        $title = "Home";
        $this->load->view('includes/header', array('title' => $title));
        $this->load->view('pages/home');
        $this->load->view('includes/footer');
    }
        
    public function about(){
        $title = "About Us";
        $this->load->view('includes/header', array('title' => $title));
        $this->load->view('pages/about');
        $this->load->view('includes/footer');
    }
}
