<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title><?= ucwords(strtolower($title)); ?></title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Favicons -->
        <link href="<?= base_url('assets/img/favicon.png') ?>" rel="icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:300,400,500,700,800" rel="stylesheet">

        <!-- Bootstrap CSS File -->
        <link href="<?= base_url('assets/lib/bootstrap/css/bootstrap.css') ?>" rel="stylesheet">

        <!-- Libraries CSS Files -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?= base_url('assets/lib/font-awesome/css/font-awesome.css') ?>" rel="stylesheet">
        <link href="<?= base_url('assets/lib/animate/animate.css') ?>" rel="stylesheet">
        <link href="<?= base_url('assets/lib/venobox/venobox.css') ?>" rel="stylesheet">
        <link href="<?= base_url('assets/lib/owlcarousel/assets/owl.carousel.css') ?>" rel="stylesheet">
        <!-- Swiper CSS -->
        <link href="<?= base_url('assets/css/swiper.css') ?>" rel="stylesheet">

        <!-- Styles -->
        <link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">

        <!-- Custom Stylesheet File -->
        <link href="<?= base_url('assets/css/custom_style.css') ?>" rel="stylesheet">

    </head>

    <body>

        <header class="site-header">
            <div class="nav-bar">
                <div class="container">
                    <div class="row">
                        <div class="col-12 d-flex flex-wrap justify-content-between align-items-center">
                            <div class="site-branding d-flex align-items-center">
                                <!--                                <h1>Ashtang Chikitsalay</h1>-->
                                                                <!--<a class="d-block" href="index.html" rel="home"><img class="d-block" src="images/logo.png" alt="logo"></a>-->
                            </div><!-- .site-branding -->

                            <nav class="site-navigation d-flex justify-content-end align-items-center">
                                <ul class="d-flex flex-column flex-lg-row justify-content-lg-end align-items-center">
                                    <li class="current-menu-item"><a href="<?php echo base_url('home') ?>">होम</a></li>
                                    <li><a href="about.html">विवरण</a></li>
                                    <li><a href="services.html">हमारी सेवाएँ</a></li>
                                    <li><a href="contact.html">संपर्क</a></li>

                                    <li class="call-btn button gradient-bg mt-3 mt-md-0">
                                        <span class="d-flex justify-content-center align-items-center"><img src="<?= base_url('assets/images/emergency-call.png') ?>"> +34 586 778 8892</span>
                                    </li>
                                </ul>
                            </nav><!-- .site-navigation -->

                            <div class="hamburger-menu d-lg-none">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </div><!-- .hamburger-menu -->
                        </div><!-- .col -->
                    </div><!-- .row -->
                </div><!-- .container -->
            </div><!-- .nav-bar -->
        </header><!-- .site-header -->

        

