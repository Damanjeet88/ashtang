<div class="swiper-container hero-slider">
    <div class="swiper-wrapper">
        <div class="swiper-slide hero-content-wrap" style="height:100vh; background-image: url('<?php echo base_url('assets/images/hero.jpg') ?>')">
            <div class="hero-content-overlay position-absolute w-100 h-100">
                <div class="container h-100">
                    <div class="row h-100">
                        <div class="col-12 col-lg-6 d-flex flex-column justify-content-center align-items-start">
                            <header class="entry-header">
                                <!-- <h1>अष्टांग <br>चिकित्सालय</h1> -->
                                <img style="width:375px" src="<?php echo base_url('assets/images/brandName.png'); ?>" alt="">
                            </header><!-- .entry-header -->

                            <div class="entry-content mt-5">
                                <h2 style="font-weight: 500; color: #7c440b;">पंचकर्म चिकित्सा केंद्र</h2>
                                <h5 style="font-weight: 500; color: #7c440b;">नरसिंहपुर, मध्य प्रदेश</h5>
                            </div><!-- .entry-content -->

                            <div class="entry-footer d-flex flex-wrap align-items-center mt-4">
                                <a href="#" class="button gradient-bg">Read More</a>
                            </div><!-- .entry-footer -->
                        </div><!-- .col -->
                    </div><!-- .row -->
                </div><!-- .container -->
            </div><!-- .hero-content-overlay -->
        </div><!-- .hero-content-wrap -->

        <div class="swiper-slide hero-content-wrap" style="background-image: url('images/hero.jpg')">
            <div class="hero-content-overlay position-absolute w-100 h-100">
                <div class="container h-100">
                    <div class="row h-100">
                        <div class="col-12 col-lg-6 d-flex flex-column justify-content-center align-items-start">
                            <header class="entry-header">
                                <h1>The Best <br>Medical Services</h1>
                            </header><!-- .entry-header -->

                            <div class="entry-content mt-4">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus.</p>
                            </div><!-- .entry-content -->

                            <footer class="entry-footer d-flex flex-wrap align-items-center mt-4">
                                <a href="#" class="button gradient-bg">Read More</a>
                            </footer><!-- .entry-footer -->
                        </div><!-- .col -->
                    </div><!-- .row -->
                </div><!-- .container -->
            </div><!-- .hero-content-overlay -->
        </div><!-- .hero-content-wrap -->

        <div class="swiper-slide hero-content-wrap" style="background-image: url('images/hero.jpg')">
            <div class="hero-content-overlay position-absolute w-100 h-100">
                <div class="container h-100">
                    <div class="row h-100">
                        <div class="col-12 col-lg-6 d-flex flex-column justify-content-center align-items-start">
                            <header class="entry-header">
                                <h1>The Best <br>Medical Services</h1>
                            </header><!-- .entry-header -->

                            <div class="entry-content mt-4">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec malesuada lorem maximus mauris scelerisque, at rutrum nulla dictum. Ut ac ligula sapien. Suspendisse cursus faucibus finibus.</p>
                            </div><!-- .entry-content -->

                            <div class="entry-footer d-flex flex-wrap align-items-center mt-4">
                                <a href="#" class="button gradient-bg">Read More</a>
                            </div><!-- .entry-footer -->
                        </div><!-- .col -->
                    </div><!-- .row -->
                </div><!-- .container -->
            </div><!-- .hero-content-overlay -->
        </div><!-- .hero-content-wrap -->
    </div><!-- .swiper-wrapper -->

    <div class="pagination-wrap position-absolute w-100">
        <div class="swiper-pagination d-flex flex-row flex-md-column"></div>
    </div><!-- .pagination-wrap -->
</div><!-- .hero-slider -->


<div class="container">
    <div class="row">
        <div class="col-sm text-center mt-5">
            <h1 class="row-title mb-3" style="font-family: KrutiDev;">पंचकर्म सामान्य परिचय</h1 class="row-title">
        </div>
    </div>
    <div class="row">
        <div class="col-sm text-center">
            <h5>
                पंचकर्म के अंतर्गत पहले सम्पूर्ण शरीर का शोधन किया जाता है | 
            </h5>
            <h5>
                सामान्य रूप से इस प्रक्रिया  के अंतर्गत निम्न उपक्रम किये जाते हैं |
            </h5>
        </div>
    </div>
    <div class="row">
        <div class="col-sm mt-5">
            <div class="tab-container effect2">

                <div class="tabs-list">
                    <!---tab-link class for multiple tabs---->
                    <div class="tab-link active">स्नेहन</div>
                    <div class="tab-link">स्वेदन (स्टीम - बाथ)</div>
                    <div class="tab-link">विरेचन </div>
                    <div class="tab-link">उत्तर वस्ति</div>
                    <div class="tab-link">नस्य </div>
                    <div class="tab-link">शिरोधारा </div>
                    <div class="tab-link">वस्ति </div>
                    <div class="tab-link">वमन</div>
                </div>

                <div class="tab-content-area" style="position: relative">
                    <!---tab-content class for individual content---->
                    <div class="service-tab-content active">
                        <div class="position-absolute" >
                            <h2 class="tab-header">स्नेहन</h2>
                            <p>बाह्य रूप से शरीर को औषधि युक्त तेलों से मालिश  कर शरीर के दोषों को तथा औषधि युक्त घृत का सेवन कर अन्दर के दोषों को ढीला करता है |
                            </p>
                        </div>

                        <div class="service-img">
                            <img src="<?php echo base_url('assets/images/tabs-images/snehan.jpg') ?>" alt="">
                        </div>
                    </div>
                    <div class="service-tab-content">
                        <div class="position-absolute" >
                            <h2 class="tab-header">स्वेदन (स्टीम - बाथ)</h2>
                            <p>वाष्पन द्वारा पसीने से दोषों को बहार निकलते हैं | इससे मोटापा शरीर की जकड़न , जोड़ों का दर्द , आदि में  विशेष लाभ मिलता है |</p>
                        </div>

                        <div class="service-img">
                            <img src="<?php echo base_url('assets/images/tabs-images/svedan.jpg') ?>" alt="">
                        </div>
                    </div>

                    <div class="service-tab-content">
                        <div class="position-absolute" >
                            <h2 class="tab-header">विरेचन </h2>
                            <p>पाचन सम्बन्धी विकारों को दस्त द्वारा बाहर  निकालना , पुराने कठिन पित्त सम्बन्धी रोग , त्वचा सम्बंधित रोग , पेट से सम्बंधित रोग आदि में विशेष लाभकारी |</p>
                        </div>


                        <div class="service-img">
                            <img src="<?php echo base_url('assets/images/tabs-images/virechan.jpg') ?>" alt="">
                        </div>
                    </div>
                    <div class="service-tab-content">
                        <div class="position-absolute" >
                            <h2 class="tab-header">उत्तर वस्ति </h2>
                            <p>महिलाओं को प्रदर आदि यौन रोगों की विशेष चिकित्सा |</p>
                        </div>

                        <div class="service-img">
                            <img src="<?php echo base_url('assets/images/tabs-images/uttar-vasti.jpg') ?>" alt="">
                        </div>
                    </div>
                    <div class="service-tab-content">
                        <div class="position-absolute">
                            <h2 class="tab-header">नस्य</h2>
                            <p>नाक द्वारा औषधि का प्रयोग कराके विभिन्न प्रकार के नासा रोग एवं शिरा रोगों की चिकित्सा |</p>
                        </div>

                        <div class="service-img">
                            <img src="<?php echo base_url('assets/images/tabs-images/nasya.jpg') ?>" alt="">
                        </div>
                    </div>
                    <div class="service-tab-content">
                        <div class="position-absolute" >
                            <h2 class="tab-header">शिरोधारा </h2>
                            <p>विशेष गति से औषधियुक्त तेल क्वाथ दुग्ध , अर्क आदि से शिर का बुर्पण किया जाता है | विभिन्न प्रकार के शिरा रोग माई ग्रेन , डिप्रेशन , अनिद्रा  तथा बालों का पकना , झड़ना आदि में विशेष लाभप्रद है |</p>
                        </div>

                        <div class="service-img">
                            <img src="<?php echo base_url('assets/images/tabs-images/shirodhara.jpg') ?>" alt="">
                        </div>
                    </div>
                    <div class="service-tab-content">
                        <div class="position-absolute" >
                            <h2 class="tab-header">वस्ति</h2>
                            <p>ऐनिमा द्वारा औषधि युक्त क्वाथ तेल आदि से वात रोगों का शमन एवं शरीर का पोषण , घुटने के रोगों के लिए जानुवस्ति , कमर के रोगों के लिए कटीवस्ति , नेत्र के रोगों के लिए नेत्रवस्ति  | इस प्रकार विशिष्ट औषधियों से युक्त एक पोटली तैयार की जाती है | जिसे पत्र पिण्ड स्वेद कहते हैं यह सभी प्रकार के दर्द एवं सूजन में अत्यंत लाभप्रद है |</p> 
                        </div>

                        <div class="service-img">
                            <img src="<?php echo base_url('assets/images/tabs-images/vasti.jpg') ?>" alt="">
                        </div>
                    </div>
                    <div class="service-tab-content">
                        <div class="position-absolute" >
                            <h2 class="tab-header">वमन </h2>
                            <p>औषधि युक्त पेय से पेट के ऊपर की दोष , फेफड़े के दोष , कफ से होने वाले रोगों का नाश करना |</p>
                        </div>

                        <div class="service-img">
                            <img src="<?php echo base_url('assets/images/tabs-images/vaman.jpg') ?>" alt="">
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>