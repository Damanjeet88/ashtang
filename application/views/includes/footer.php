<!--==========================
    Footer
  ============================-->
<footer id="footer" class="mt-5">
    <div class="footer-top">
        <div class="container">
            <div class="row ">

                <div class="col-lg-3 col-md-6 footer-info">
                    <h1 class="font-weight-bold" style="color:#fff; font-size: 20px">अष्टांग चिकित्सालय</h1>
                    <p>City Auto Glass is one stop solution for all your auto glass and windshield repair and replacement necessities.</p>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="fa fa-angle-right"></i> <a href="<?= base_url(); ?>">Home</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="<?= base_url('home/about') ?>">About us</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="<?= base_url('home/services') ?>">Services</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="<?= base_url('home/contact') ?>">Contact Us</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Privacy policy</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <ul>
                        <li><i class="fa fa-angle-right"></i> <a href="<?= base_url('home/services#faq'); ?>">F.A.Q</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="<?= base_url('home/contact'); ?>">Book An Appointment</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="<?= base_url('home/contact'); ?>">Customer experience</a></li>
<!--                        <li><i class="fa fa-angle-right"></i> <a href="#">Terms of service</a></li>
                        <li><i class="fa fa-angle-right"></i> <a href="#">Privacy policy</a></li>-->
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 footer-contact">
                    <h4>अष्टांग चिकित्सालय पंचकर्म  चिकित्सा केंद्र </h4>
                    <p>
                    इतवारा बाज़ार,  <br>
                    नरसिंह पुर(मध्य प्रदेश )<br>
                        
                        <strong>Phone:</strong> 07792-230934<br>
                        <strong>Email:</strong> info@example.com<br>
                    </p>

                    <div class="social-links">
                        <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    </div>

                </div>

            </div>
        </div>
    </div>


</footer><!-- #footer -->

<!-- JavaScript Libraries -->
<script src="<?= base_url('assets/lib/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/lib/jquery/jquery-migrate.min.js') ?>"></script>
<script src="<?= base_url('assets/lib/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?= base_url('assets/lib/easing/easing.min.js') ?>"></script>
<script src="<?= base_url('assets/lib/superfish/hoverIntent.js') ?>"></script>
<script src="<?= base_url('assets/lib/superfish/superfish.min.js') ?>"></script>
<script src="<?= base_url('assets/lib/wow/wow.min.js') ?>"></script>
<script src="<?= base_url('assets/lib/venobox/venobox.min.js') ?>"></script>
<script src="<?= base_url('assets/lib/owlcarousel/owl.carousel.min.js') ?>"></script>

<!-- Contact Form JavaScript File -->
<!--<script src="<?= base_url('assets/contactform/contactform.js') ?>"></script>-->

<!-- Template Main Javascript File -->
<!--<script src="<?= base_url('assets/js/main.js') ?>"></script>-->


<script>
    $('.tab-link').click(function () {
        $('.tab-link').removeClass('active');
        $(this).addClass('active');
        //getting the index of clicked tab and using it to show corresponding service-tab-content
        var tab_index = $('.tab-link').index($(this));
        $('.service-tab-content').removeClass('active');
        $('.service-tab-content').eq(tab_index).addClass('active');

    });
</script>
</body>

</html>