<?php

class MY_Controller extends CI_Controller {

    public function ca() {
        $c = strtolower($this->router->fetch_class());
        $a = strtolower($this->router->fetch_method());
        return $c . "." . $a;
    }
    
    public function _Json($status) {
        return $this->output
                        ->set_content_type('application/json')
                        ->set_header('Access-Control-Allow-Origin:*')
                        ->set_header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE')
                        ->set_header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With')
                        ->set_output(json_encode($status));
    }
    public function alpha_dash_space($authorName) {
        if (preg_match("/^[a-z ,.'-]+$/i", $authorName)) {
            return TRUE;
        } else {
            $this->form_validation->set_message("alpha_dash_space", "Invalid {field} Name");
            return FALSE;
        }
    }

}
